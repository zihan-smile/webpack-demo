# webpack-demo

#### 项目介绍
webpack 使用示例

``` bash
# 克隆到本地
git clone https://gitee.com/zihan-smile/webpack-demo.git

# webpack4 使用示例
cd webpack-demo/webpack4-demo

# webpack4 + vue2 + vue-router 使用示例
cd webpack-demo/webpack4-vue

# 请注意个文件夹下面的README.md
```

