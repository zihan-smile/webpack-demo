# webpack4 + vue2 + vue-router 使用示例

## Build Setup

``` bash
# 克隆到本地
git clone https://gitee.com/zihan-smile/webpack-demo.git

# 进入文件夹
cd webpack-demo/webpack4-vue

# 安装相关依赖
npm install

# 开启本地服务器
npm run dev

# 浏览器里面打开下面地址
http://localhost:8250/

# 编译生产环境代码
npm run build

```