/**
 * ==================
 * RULES
 * ==================
 */
const autoprefixer = require('autoprefixer')
const MiniCssPlugin = require('mini-css-extract-plugin')
const postcssLoader = {
    loader: 'postcss-loader',
    options: {
        plugins: () => [autoprefixer]
    }
}


function rules(options) {
    options = options || {}
    if (Object.prototype.toString.call(options) !== "[object Object]") {
        return
    }
    let {
        dev = true
    } = options
    const ruleLoaders = {
        babel: {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    // 设置为true将缓存 loader 的执行结果，加快编译速度
                    cacheDirectory: true
                }
            }
        },
        // eslint: {
        //     test: /\.(js|vue)$/,
        //     loader: 'eslint-loader',
        //     enforce: 'pre',
        //     exclude: /node_modules/,
        //     options: {
        //         formatter: require('eslint-friendly-formatter')
        //     }
        // },
        json: {
            test: /\.json$/,
            use: 'json-loader'
        },
        css: {
            test: /\.css$/,
            use: [dev ? 'style-loader' : MiniCssPlugin.loader, 'css-loader', postcssLoader]
        },
        scss: {
            test: /\.scss$/,
            use: [dev ? 'style-loader' : MiniCssPlugin.loader, 'css-loader', postcssLoader, 'sass-loader']
        },
        image: {
            test: /\.(jpg|png|gif|ico)(\?.*)?(#.*)?$/,
            use: {
                loader: 'url-loader',
                options: {
                    name: dev ? '[name].[ext]' : '[name].[hash].[ext]',
                    limit: 4096
                }
            }
        },
        font: {
            test: /\.(svg|eot|ttf|woff|woff2)(\?.*)?(#.*)?$/,
            use: {
                loader: 'file-loader',
                options: {
                    name: dev ? '[name].[ext]' : '[name].[hash].[ext]'
                }
            }
        },
        vue: {
            test: /\.vue$/,
            use: {
                loader: 'vue-loader',
                options: {
                    // postcss: [autoprefixer]
                }
            }
        },
        ts: {
            test: /\.tsx?$/,
            use: {
                use: 'ts-loader',
                exclude: /node_modules/
            }
        }
    }

    return ruleLoaders
}

/**
 * ==================
 * PLUGINS
 * ==================
 */
function plugins(options) {
    let {
        miniCss = {}, htmlWebpack = {}
    } = options
    return {
        miniCssPlugin: new MiniCssPlugin({
            miniCss
        }),
        htmlWebpackPlugin: new HtmlWebpackPlugin(htmlWebpack)
    }
}

function splitChunks(options) {
    options = options || {}
    let {
        minSize = 30000, chunks = 'all', minChunks = 1, maxAsyncRequests = 5, maxInitialRequests = 3
    } = options
    let config = {
        chunks, // 控制webpack选择哪些代码块用于分割（其他类型代码块按默认方式打包）。有3个可选的值：initial、async和all。
        minSize, // 形成一个新代码块最小的体积
        maxSize: 0,
        minChunks, // 在分割之前，这个代码块最小应该被引用的次数（默认配置的策略是不需要多次引用也可以被分割）
        maxAsyncRequests, // 按需加载的代码块，最大数量应该小于或者等于5
        maxInitialRequests, // 初始加载的代码块，最大数量应该小于或等于3
        automaticNameDelimiter: '~',
        name: true,
        cacheGroups: {
            vendors: { // 将所有来自node_modules的模块分配到一个叫vendors的缓存组
                test: /[\\/]node_modules[\\/]/,
                // name(module) {  // 如果需要对每一个node_modules下面的npm包进行单独打包，可以使用下面这个方法
                //     // get the name. E.g. node_modules/packageName/not/this/part.js
                //     // or node_modules/packageName
                //     const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                //     // npm package names are URL-safe, but some servers don't like @ symbols
                //     return `npm.${packageName.replace('@', '')}`;
                // },
                priority: -10 // 缓存组的优先级(priotity)是负数，因此所有自定义缓存组都可以有比它更高优先级
            },
            default: {
                name: 'common',
                minSize: 1,
                minChunks: 2, // 所有重复引用至少两次的代码，会被分配到default的缓存组。
                priority: -20, // 一个模块可以被分配到多个缓存组，优化策略会将模块分配至跟高优先级别（priority）的缓存组
                reuseExistingChunk: true // 允许复用已经存在的代码块，而不是新建一个新的，需要在精确匹配到对应模块时候才会生效。
            }
        }
    }
    return config
}

module.exports = {
    rules,
    plugins,
    splitChunks,
    port: 8250
}