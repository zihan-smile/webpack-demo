import Vue from 'vue'
import './css/base.css'
import router from './router'

import App from './App.vue'

new Vue({
    el: "#app",
    router,
    render: h => h(App)
})