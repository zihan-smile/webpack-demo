import Vue from 'vue'
import Router from 'vue-router'
import list from '@/components/list'
// import foo from '@/components/foo'
Vue.use(Router)

export default new Router({
    // mode: 'history',
    routes: [
        {
            path: '*',
            component: list
        },
        {
            path: '/',
            component: list
        },
        {
            path: '/list',
            component: list
        },
        {
            path: '/foo',
            component: () => import(/* webpackChunkName: "foo" */ '@/components/foo')
        }
    ],
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
})
