const getSerializeString = (obj) => {
    var objectArray = [];
    for(var i in obj) {
        if (obj.hasOwnProperty(i)){
            var tempItem = obj[i];
            objectArray.push(i + "=" + tempItem);
        }
    }
    return objectArray.join("&");
}


/**
 * ajax 请求封装 (采用了es6的一些语法)
 * @param  {[Object]} options [参数对象]
 * @param  {[String]} options.url [发送请求的URL]
 * @param  {[Object]} options.data [请求的参数]
 * @param  {[String]} options.method [请求的方法，默认为get]
 * @param  {[Object]} options.headers [头部header设置]
 * @param  {[Function]} options.success [获取数据成功后的回调函数]
 * @param  {[Function]} options.error [获取数据失败后的回调函数，会传递xhr对象进去
 * @param  {[Boolean]} options.withCredentials [是否跨域设置，默认false]
 * @param  {[String]} options.responseType 返回响应数据的类型,如'text|arraybuffer|json',根据需要设置，默认为text
 */
export const http = (options) => {
    if (Object.prototype.toString.call(options) !== '[object Object]') return
    let {
        url = null, method = 'get', withCredentials = false,
            data = {}, headers = {}, success, error
    } = options
    if (!url) {
        return
    }
    console.log('send data ……')
    let connectors = ~url.indexOf('?') ? '&' : '?'
    // 如果是get请求，则把参数拼接在url上面 getSerializeString方法参考上面实现
    method = method.toLowerCase()
    method === 'get' && data && (url = url + connectors + getSerializeString(data))
    let ajax = null
    if (window.XMLHttpRequest) {
        ajax = new XMLHttpRequest()
    } else {
        ajax = new ActiveXObject("Microsoft.XMLHTTP")
    }
    ajax.open(method, url, true)
    // 处理headers
    headers = Object.assign({}, {
        'Content-type': "application/json"
    }, headers)
    if (options.responseType) {
        ajax.responseType = options.responseType
    }
    for (let key in headers) {
        ajax.setRequestHeader(key, headers[key])
    }

    ajax.withCredentials = withCredentials
    ajax.send(method === 'get' ? null : data)
    ajax.onreadystatechange = () => {
        // readyState 为4 说明已经接收到了完整的响应
        if (ajax.readyState === 4) {
            // 200表示http的状态请求正常
            if (ajax.status === 200) {
                // 这里处理接收到的数据
                let response = ajax.response || ajax.responseText
                success && success(response)
            } else {
                error && error(ajax)
            }
        }
    }
    // 这里也可以使用ajax.onload方法来处理
}