/**
 * ==================
 * RULES
 * ==================
 */
const autoprefixer = require('autoprefixer')
const MiniCssPlugin = require('mini-css-extract-plugin')
const postcssLoader = {
    loader: 'postcss-loader',
    options: {
        plugins: () => [autoprefixer]
    }
}

function rules(options) {
    options = options || {}
    if (Object.prototype.toString.call(options) !== "[object Object]") {
        return
    }
    let {
        dev = true
    } = options
    const ruleLoaders = {
        babel: {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader'
            }
        },
        // eslint: {
        //     test: /\.(js|vue)$/,
        //     loader: 'eslint-loader',
        //     enforce: 'pre',
        //     exclude: /node_modules/,
        //     options: {
        //         formatter: require('eslint-friendly-formatter')
        //     }
        // },
        json: {
            test: /\.json$/,
            use: 'json-loader'
        },
        css: {
            test: /\.css$/,
            use: [dev ? 'style-loader' : MiniCssPlugin.loader, 'css-loader', postcssLoader]
        },
        scss: {
            test: /\.scss$/,
            use: dev ? ['style-loader', 'css-loader', postcssLoader, 'sass-loader'] : [MiniCssPlugin.loader, 'css-loader', postcssLoader, 'sass-loader']
        },
        image: {
            test: /\.(jpg|png|gif|ico)(\?.*)?(#.*)?$/,
            use: {
                loader: 'url-loader',
                options: {
                    name: dev ? '[name].[ext]' : '[name].[hash].[ext]',
                    limit: 4096
                }
            }
        },
        font: {
            test: /\.(svg|eot|ttf|woff|woff2)(\?.*)?(#.*)?$/,
            use: {
                loader: 'file-loader',
                options: {
                    name: dev ? '[name].[ext]' : '[name].[hash].[ext]'
                }
            }
        },
        vue: {
            test: /\.vue$/,
            use: {
                loader: 'vue-loader',
                options: {
                    postcss: [autoprefixer]
                }
            }
        },
        ts: {
            test: /\.tsx?$/,
            use: {
                use: 'ts-loader',
                exclude: /node_modules/
            }
        }
    }

    return ruleLoaders
}

/**
 * ==================
 * PLUGINS
 * ==================
 */
function plugins (options) {
    let {miniCss = {}, htmlWebpack = {}} = options
    return {
        miniCssPlugin : new MiniCssPlugin({miniCss}),
        htmlWebpackPlugin: new HtmlWebpackPlugin(htmlWebpack)
    }
}

module.exports = {
    rules,
    plugins,
    port: 8210
}