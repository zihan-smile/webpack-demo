import '../css/main.css'
import {say} from './helloword'
import {http} from '../../common/utils'



// Test ES7 Async Await
function getCount(data) {
    let p = new Promise((resolve, reject) => {
        resolve(data)
    })
    return p
}

async function showData() {
    let data = await getCount(10)
    document.querySelector('#resultAsync').innerHTML = data
}

showData()

// Test Tree-Shaking
say()

// Test build remove console.log
console.log('log')
console.error('error')

// Test Api Request
http({
    url: '/api/types',
    success: (res) => {
        document.querySelector('#result').innerHTML = typeof res === 'string' ? res : JSON.stringify(res)
    }
})